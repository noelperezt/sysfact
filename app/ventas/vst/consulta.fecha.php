<?php require '../../../cfg/base.php'; ?>
<form class="form-horizontal insert">
	<?php echo $fn->modalHeader('Consultar Fecha') ?>
	<div class="modal-body">
		<div class="form-group col-sm-12">
			<label class="control-label col-sm-3 bolder">
				Fecha:
			</label>
		<div class="input-group date" data-provide="datepicker" >
    <input value = <?php echo date("m/d/Y");?> type="text" class="form-control" name="fechas" id ="fechas">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>

</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		        <button type="button" class="btn btn-primary" onclick= "consultar()">Aceptar</button>
	</div>
</form>

<script>
	
		function consultar() {
		var usr = $('#fechas').val();
		var fecha = usr.split('/')
				cerrarmodal()
				window.open('rpt-ventasf-mes='+fecha[0]+'&dia='+fecha[1]+'&anio='+fecha[2]);
		}
	
</script>